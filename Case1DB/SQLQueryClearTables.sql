﻿Delete from Authors Where AuthorId > 0
GO
Delete from Publishers Where PublisherId > 0
GO
Delete from Genres Where GenreId > 0
GO
Delete from Product Where ProductId > 0
GO
Delete from CartItems Where CartItemId > 0
GO
Delete from Carts Where CartId > 0
GO 