﻿namespace ASPNetCase1.Utils
{
    public static class SessionVars
    {
        // place all session variables for application here
        public const string LoginStatus = "loginstatus";
        public const string User = "user";
        public const string Message = "message";
        public const string Cart = "cart";
        public const string Catalogue = "catalogue";
        public const string Genres = "genres";
    }
}