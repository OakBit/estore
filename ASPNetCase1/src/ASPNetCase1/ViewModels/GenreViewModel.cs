﻿using Microsoft.AspNet.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;
using ASPNetCase1.Models;
namespace ASPNetCase1.ViewModels
{
    public class GenreViewModel
    {
        public GenreViewModel() { }
        public string GenreName { get; set; }
        public List<Genre> Genres { get; set; }
        public IEnumerable<Product> Products { get; set; }
        public IEnumerable<SelectListItem> GetGenreNames()
        {
            return Genres.Select(genre => new SelectListItem { Text = genre.Name, Value = genre.Name });
        }
    }
}