﻿namespace ASPNetCase1.ViewModels
{
    public class CartViewModel
    {
        public int CartId { get; set; }
        public string UserId { get; set; }
        public string ProductName { get; set; }
        public decimal MSRP { get; set; }
        public int QtyS { get; set; }
        public int QtyO { get; set; }
        public int QtyB { get; set; }
        public decimal Extended { get; set; }

        public string DateCreated { get; set; }
        public decimal TotalCostPrice { get; set; }
        public decimal TotalMSRP { get; set; }
        public decimal TotalTax { get; set; }
        public decimal Total { get; set; }

    }
}