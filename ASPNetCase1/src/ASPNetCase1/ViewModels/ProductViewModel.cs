﻿using ASPNetCase1.Models;
using System.Collections.Generic;
namespace ASPNetCase1.ViewModels
{
    public class ProductViewModel
    {
        public string GenreName { get; set; }
        public int GenreId { get; set; }
        public IEnumerable<Product> Products { get; set; }
        public string ProductId { get; set; }
        public string Description { get; set; }
        public int Qty { get; set; }
        public string ProductName { get; set; }
        public string AuthorName { get; set; }
        public int AuthorId { get; set; }
        public string PublisherName { get; set; }
        public int PublisherId { get; set; }
        public decimal CostPrice { get; set; }
        public string GENRE { get; set; }
        public decimal MSRP { get; set; }
        public int QtyOnHand { get; set; }//should qty on hand and backorder be excluded and checked in controller? if there is enough available
        public int QtyOnBackOrder { get; set; }
        public string ISBN10 { get; set; }
        public string ISBN13 { get; set; }
        public string GraphicName { get; set; }
        public string JsonData { get; set; }
    }
}