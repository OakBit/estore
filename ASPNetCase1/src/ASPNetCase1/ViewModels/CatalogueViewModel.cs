﻿using ASPNetCase1.Models;
using Microsoft.AspNet.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
namespace ASPNetCase1.ViewModels
{
    public class CatalogueViewModel
    {
        private List<Genre> _genres;
        [Required]
        public int Qty { get; set; }
        public string ProductId { get; set; }
        ///
        public int GenreId { get; set; }
        public IEnumerable<SelectListItem> GetGenres()
        {
            return _genres.Select(genre => new SelectListItem { Text = genre.Name, Value = Convert.ToString(genre.GenreId) });
        }

        public void SetGenres(List<Genre> gens)
        {
            _genres = gens;
        }
    }
}