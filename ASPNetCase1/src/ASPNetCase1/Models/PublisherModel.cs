﻿using System.Collections.Generic;
using System.Linq;
namespace ASPNetCase1.Models
{
    public class PublisherModel
    {
        private AppDbContext _db;
        public PublisherModel(AppDbContext ctx)
        {
            _db = ctx;
        }
        public List<Publisher> GetAll()
        {
            return _db.Publishers.ToList<Publisher>();
        }
        public string GetPublisherName(int publisherid)
        {
            Publisher publisher = new Publisher();
            publisher = _db.Publishers.FirstOrDefault(pub => pub.PublisherId == publisherid);
            return publisher.Name;
        }
    }
}