﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Data.Entity;
using ASPNetCase1.ViewModels;
using ASPNetCase1.Models;
using System.Threading.Tasks;

namespace ASPNetCase1.Models
{
    public class CartModel
    {
        private AppDbContext _db;
        public CartModel(AppDbContext ctx)
        {
            _db = ctx;
        }
        public int AddCart(Dictionary<string, object> items, string user)
        {
            int cartId = -1;
            using (_db)
            {
                // we need a transaction as multiple entities involved
                using (var _trans = _db.Database.BeginTransaction())
                {
                    try
                    {
                        Cart cart = new Cart();
                        cart.UserId = user;
                        cart.DateCreated = System.DateTime.Now;
                        cart.TotalCostPrice = 0;
                        cart.TotalMSRP = 0;
                        cart.TotalTax = 0;
                        // calculate the totals and then add the cart row to the table
                        foreach (var key in items.Keys)
                        {
                            ProductViewModel item = JsonConvert.DeserializeObject<ProductViewModel>(Convert.ToString(items[key]));
                            if (item.Qty > 0)
                            {
                                cart.TotalCostPrice += item.CostPrice * item.Qty;
                                cart.TotalMSRP += item.MSRP * item.Qty;
                            }
                        }
                        cart.TotalTax = cart.TotalMSRP * 0.13M;

                        _db.Carts.Add(cart);
                        _db.SaveChanges();
                        // then add each item to the cartitems table
                        foreach (var key in items.Keys)
                        {
                            ProductViewModel item = JsonConvert.DeserializeObject<ProductViewModel>(Convert.ToString(items[key]));
                            if (item.Qty > 0 && item.Qty <= item.QtyOnHand)
                            {
                                CartItem cItem = new CartItem();
                                cItem.QtySold = item.Qty;
                                cItem.QtyBackOrdered = 0;
                                cItem.QtyOrdered = item.Qty;
                                cItem.ProductId = item.ProductId;
                                cItem.CartId = cart.CartId;

                                item.QtyOnHand = item.QtyOnHand - item.Qty;//decrease the available Quantity
                                _db.CartItems.Add(cItem);
                                //update product with new quantity
                                Product tempProd = new Product();
                                tempProd.ProductId = item.ProductId;
                                tempProd.ProductName = item.ProductName;
                                tempProd.GenreId = item.GenreId;
                                tempProd.Description = item.Description;
                                tempProd.ProductId = item.ProductId;
                                tempProd.AuthorId = item.AuthorId;
                                tempProd.PublisherId = item.PublisherId;
                                tempProd.CostPrice = item.CostPrice;
                                tempProd.MSRP = item.MSRP;
                                tempProd.GraphicName = item.GraphicName;
                                tempProd.QtyOnHand = item.QtyOnHand;
                                tempProd.QtyOnBackOrder = item.QtyOnBackOrder;
                                tempProd.ISBN10 = item.ISBN10;
                                tempProd.ISBN13 = item.ISBN13;

                                _db.Products.Update(tempProd);
                                _db.SaveChanges();
                            }
                            else if(item.Qty > item.QtyOnHand)
                            {
                                CartItem cItem = new CartItem();
                                cItem.QtySold = item.Qty;
                                cItem.QtyBackOrdered = item.Qty - item.QtyOnHand;
                                cItem.QtyOrdered = item.QtyOnHand;
                                cItem.ProductId = item.ProductId;
                                cItem.CartId = cart.CartId;

                                item.QtyOnBackOrder = item.QtyOnBackOrder - item.QtyOnHand + item.Qty;
                                item.QtyOnHand = 0;//decrease the available Quantity
                                _db.CartItems.Add(cItem);

                                //update product with new quantity
                                Product tempProd = new Product();
                                tempProd.ProductId = item.ProductId;
                                tempProd.ProductName = item.ProductName;
                                tempProd.GenreId = item.GenreId;
                                tempProd.Description = item.Description;
                                tempProd.ProductId = item.ProductId;
                                tempProd.AuthorId = item.AuthorId;
                                tempProd.PublisherId = item.PublisherId;
                                tempProd.CostPrice = item.CostPrice;
                                tempProd.MSRP = item.MSRP;
                                tempProd.GraphicName = item.GraphicName;
                                tempProd.QtyOnHand = item.QtyOnHand;
                                tempProd.QtyOnBackOrder = item.QtyOnBackOrder;
                                tempProd.ISBN10 = item.ISBN10;
                                tempProd.ISBN13 = item.ISBN13;

                                _db.Products.Update(tempProd);
                                _db.SaveChanges();
                            }
                        }
                        // test trans by uncommenting out these 3 lines
                        //int x = 1;
                        //int y = 0;
                        //x = x / y;
                        _trans.Commit();
                        cartId = cart.CartId;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        _trans.Rollback();
                    }
                }
            }
            return cartId;
        }
        public List<Cart> GetCarts(string uid)
        {
            try
            {
                return _db.Carts.Where(c => c.UserId == uid).ToList();
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return null;
            }
        }
        public List<CartViewModel> GetCartDetails(int cid, string uid)
        {
            List<CartViewModel> allDetails = new List<CartViewModel>();
            // LINQ way of doing INNER JOINS
            var results = from c in _db.Set<Cart>()
                          join ci in _db.Set<CartItem>() on c.CartId equals ci.CartId
                          join pi in _db.Set<Product>() on ci.ProductId equals pi.ProductId
                          where (c.UserId == uid && c.CartId == cid)
                          select new CartViewModel
                          {
                              CartId = ci.CartId,
                              UserId = uid,
                              ProductName = pi.ProductName,
                              MSRP = pi.MSRP,
                              QtyB = ci.QtyBackOrdered,
                              QtyO = ci.QtyOrdered,
                              QtyS = ci.QtySold,
                              Extended = ci.QtySold * pi.MSRP,
                              TotalCostPrice = c.TotalCostPrice,
                              TotalMSRP = c.TotalMSRP,
                              TotalTax = c.TotalTax,
                              Total = c.TotalMSRP + c.TotalTax,
                              DateCreated = c.DateCreated.ToString("yyyy/MM/dd - hh:mm tt")
                          };
            allDetails = results.ToList<CartViewModel>();
            return allDetails;
        }

        public async Task<List<CartViewModel>> GetCartDetailsAsync(int cid, string uid)
        {
            List<CartViewModel> allDetails = new List<CartViewModel>();
            // LINQ way of doing INNER JOINS
            var results = from c in _db.Set<Cart>()
                          join ci in _db.Set<CartItem>() on c.CartId equals ci.CartId
                          join pi in _db.Set<Product>() on ci.ProductId equals pi.ProductId
                          where (c.UserId == uid && c.CartId == cid)
                          select new CartViewModel
                          {
                              CartId = ci.CartItemId,
                              UserId = uid,
                              ProductName = pi.ProductName,
                              MSRP = pi.MSRP,
                              QtyB = ci.QtyBackOrdered,
                              QtyO = ci.QtyOrdered,
                              QtyS = ci.QtySold,
                              Extended = ci.QtySold * pi.MSRP,
                              TotalCostPrice = c.TotalCostPrice,
                              TotalMSRP = c.TotalMSRP,
                              TotalTax = c.TotalTax,
                              Total = c.TotalMSRP + c.TotalTax,
                              DateCreated = c.DateCreated.ToString("yyyy/MM/dd - hh:mm tt")
                          };
            allDetails = await results.ToListAsync<CartViewModel>();
            return allDetails;
        }
    }
}