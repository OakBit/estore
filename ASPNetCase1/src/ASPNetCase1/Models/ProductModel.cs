﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace ASPNetCase1.Models
{
    public class ProductModel
    {
        /// <summary>
        ///  MenuItemModel - Model class representing a MenuItem
        ///     Author:     Evan Lauersen
        ///     Date:       Created: Feb 27, 2016
        ///     Purpose:    Model class to interface with DB and feed data to 
        ///                 Controller
        /// </summary>
        private AppDbContext _db;
        /// <summary>
        /// constructor should pass instantiated DbContext
        /// <summary>
        public ProductModel(AppDbContext context)
        {
            _db = context;
        }
        public bool loadGenres(string rawJson)
        {
            bool loadedGenres = false;
            try
            {
                // clear out the old rows
                _db.Genres.RemoveRange(_db.Genres);
                _db.SaveChanges();

                dynamic decodedJson = Newtonsoft.Json.JsonConvert.DeserializeObject(rawJson);
                List<String> allGenres = new List<String>();

                foreach (var c in decodedJson)
                {
                    allGenres.Add(Convert.ToString(c["GENRE"]));  
                }

                // distinct will remove duplicates before we insert them into the db
                IEnumerable<String>genres = allGenres.Distinct<String>();

                foreach (string g in genres)
                {
                    Genre gen = new Genre();
                    gen.Name = g;
                    _db.Genres.Add(gen);
                    _db.SaveChanges();
                }
                loadedGenres = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error - " + ex.Message);
            }
            return loadedGenres;
        }
        public bool loadAuthors(string rawJson)
        {
            bool loadedAuthors = false;
            try
            {
                // clear out the old rows
                _db.Authors.RemoveRange(_db.Authors);
                _db.SaveChanges();

                dynamic decodedJson = Newtonsoft.Json.JsonConvert.DeserializeObject(rawJson);
                List<String> allAuthors = new List<String>();

                foreach (var a in decodedJson)
                {
                    allAuthors.Add(Convert.ToString(a["AUT"]));
                }

                // distinct will remove duplicates before we insert them into the db
                IEnumerable<String> authors = allAuthors.Distinct<String>();

                foreach (string a in authors)
                {
                    Author aut = new Author();
                    aut.Name = a;
                    _db.Authors.Add(aut);
                    _db.SaveChanges();
                }
                loadedAuthors = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error - " + ex.Message);
            }
            return loadedAuthors;
        }

        public bool loadPublishers(string rawJson)
        {
            bool loadedPublishers = false;
            try
            {
                // clear out the old rows
                _db.Publishers.RemoveRange(_db.Publishers);
                _db.SaveChanges();

                dynamic decodedJson = Newtonsoft.Json.JsonConvert.DeserializeObject(rawJson);
                List<String> allPublishers = new List<String>();

                foreach (var p in decodedJson)
                {
                    allPublishers.Add(Convert.ToString(p["PUB"]));
                }

                // distinct will remove duplicates before we insert them into the db
                IEnumerable<String> publishers = allPublishers.Distinct<String>();

                foreach (string p in publishers)
                {
                    Publisher pub = new Publisher();
                    pub.Name = p;
                    _db.Publishers.Add(pub);
                    _db.SaveChanges();
                }
                loadedPublishers = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error - " + ex.Message);
            }
            return loadedPublishers;
        }
        public bool loadProducts(string rawJson)
        {
            bool loadedProducts = false;
            try
            {
                List<Genre> genres = _db.Genres.ToList();
                List<Author> authors = _db.Authors.ToList();
                List<Publisher> publishers = _db.Publishers.ToList();
                // clear out the old
                _db.Products.RemoveRange(_db.Products);
                _db.SaveChanges();
                string decodedJsonStr = Decoder(rawJson);
                dynamic ProductJson = Newtonsoft.Json.JsonConvert.DeserializeObject(decodedJsonStr);
                foreach (var p in ProductJson)
                {
                    Product product = new Product();
                    string s = RandomString(15);
                    product.ProductId = s;
                    product.ProductName = Convert.ToString(p["PNA"]);
                    product.GraphicName = Convert.ToString(p["GRA"]);
                    product.CostPrice = Convert.ToDecimal(p["COS"]);
                    product.MSRP = Convert.ToDecimal(p["MSR"]);
                    product.QtyOnHand = Convert.ToInt32(p["QOH"]);
                    product.QtyOnBackOrder = Convert.ToInt32(p["QOB"]);
                    //product.Author = Convert.ToString(p["AUT"]);
                    //product.Publisher = Convert.ToString(p["PUB"]);
                    product.ISBN10 = Convert.ToString(p["I10"]);
                    product.ISBN13 = Convert.ToString(p["I13"]);

                    string gen = Convert.ToString(p["GENRE"]);
                    string aut = Convert.ToString(p["AUT"]);
                    string pub = Convert.ToString(p["PUB"]);

                    foreach (Genre genre in genres)
                    {
                        if (genre.Name == gen)
                            product.GenreId = genre.GenreId;
                    }
                    foreach (Author author in authors)
                    {
                        if (author.Name == aut)
                            product.AuthorId = author.AuthorId;
                    }
                    foreach (Publisher publisher in publishers)
                    {
                        if (publisher.Name == pub)
                            product.PublisherId = publisher.PublisherId;
                    }

                    product.Description = Convert.ToString(p["DESC"]);

                    _db.Products.Add(product);
                    _db.SaveChanges();
                }
                loadedProducts = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error - " + ex.Message);
            }
            return loadedProducts;
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public string Decoder(string value)
        {
            Regex regex = new Regex(@"\\u(?<Value>[a-zA-Z0-9]{4})", RegexOptions.Compiled);
            return regex.Replace(value, "");
        }
        public List<Product> GetAll()
        {
            return _db.Products.ToList();
        }
        public List<Product> GetAllByBrand(int id)
        {
            return _db.Products.Where(product => product.GenreId == id).ToList();
        }
        public List<Product> GetAllByGenreName(string genname)
        {
            Genre genre = _db.Genres.First(gen => gen.Name == genname);
            return _db.Products.Where(item => item.GenreId == genre.GenreId).ToList();
        }
        public List<Product> GetAllByGenreId(int genid)
        {
            Genre genre = _db.Genres.First(gen => gen.GenreId == genid);
            return _db.Products.Where(item => item.GenreId == genre.GenreId).ToList();
        }
    }
}
