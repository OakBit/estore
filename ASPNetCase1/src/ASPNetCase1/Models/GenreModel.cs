﻿using System.Collections.Generic;
using System.Linq;
namespace ASPNetCase1.Models
{
    public class GenreModel
    {
        private AppDbContext _db;
        public GenreModel(AppDbContext ctx)
        {
            _db = ctx;
        }
        public List<Genre> GetAll()
        {
            return _db.Genres.ToList<Genre>();
        }
        public string GetGenreName(int genreid)
        {
            Genre genre = new Genre();
            genre = _db.Genres.FirstOrDefault(gen => gen.GenreId == genreid);
            return genre.Name;
        }
    }
}