﻿using System.Collections.Generic;
using System.Linq;
namespace ASPNetCase1.Models
{
    public class AuthorModel
    {
        private AppDbContext _db;
        public AuthorModel(AppDbContext ctx)
        {
            _db = ctx;
        }
        public List<Author> GetAll()
        {
            return _db.Authors.ToList<Author>();
        }
        public string GetAuthorName(int authorid)
        {
            Author author = new Author();
            author = _db.Authors.FirstOrDefault(aut => aut.AuthorId == authorid);
            return author.Name;
        }
    }
}