﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ASPNetCase1.Models
{
    public partial class Product
    {
        public Product()
        {
        }
        [Key]
        [StringLength(15)]
        public string ProductId { get; set; }
        [Required]
        public int GenreId { get; set; }
        [Required]
        [StringLength(100)]
        public string ProductName { get; set; }
        [Required]
        [StringLength(105)]
        public string GraphicName { get; set; }
        [Required]
        public int AuthorId { get; set; }
        [Column(TypeName = "money")]
        public decimal CostPrice { get; set; }
        [Column(TypeName = "money")]
        public decimal MSRP { get; set; }
        public int QtyOnHand { get; set; }
        public int QtyOnBackOrder { get; set; }
        [StringLength(2500)]
        public string Description { get; set; }

        public int PublisherId { get; set; }
        [StringLength(10)]
        public string ISBN10 { get; set; }
        [StringLength(13)]
        public string ISBN13 { get; set; }
        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] Timer { get; set; }
        [ForeignKey("GenreId")]
        public Genre Genre { get; set; }
        [ForeignKey("AuthorId")]
        public Author Author { get; set; }
        [ForeignKey("PublisherId")]
        public Publisher Publisher { get; set; }
    }
}
