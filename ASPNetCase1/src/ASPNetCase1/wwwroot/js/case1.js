﻿$(function () {
    // display message if modal still loaded i
    if ($("#detailsId").val() != "") {
        var Id = $("#detailsId").val();
        CopyToModal(Id);
        $('#details_popup').modal('show');
    } //details
    // details anchor click - to load popup on catalogue
    $("a.btn-default").on("click", function (e) {
        var Id = $(this).attr("data-id");
        $("#results").text("");
        CopyToModal(Id);
    });

    if ($("#register_popup") != undefined) {
        $('#register_popup').modal('show');
    }

    if ($("#login_popup") != undefined) {
        $('#login_popup').modal('show');
    }
});

function CopyToModal(id) {
    $("#productName").text($("#mproductName" + id).val());
    $("#qty").val("0");
    $("#author").text($("#mauthor" + id).val());
    $("#genre").text($("#mgenre" + id).val());
    $("#publisher").text($("#mpublisher" + id).val());
    $("#description").text($("#mdescription" + id).val());
    $("#price").text("$" + $("#mprice" + id).val());
    $("#isbn10").text($("#misbn10" + id).val());
    $("#isbn13").text($("#misbn13" + id).val());

    $("#detailsGraphic").attr("src", "/img/" + $("#mgraphic" + id).val());
    $("#detailsId").val(id);
}