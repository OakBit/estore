﻿//
// ReactBootstrap Component variables
//
var ListGroup = ReactBootstrap.ListGroup;
var ListGroupItem = ReactBootstrap.ListGroupItem;
var Modal = ReactBootstrap.Modal;

// example of how to do inline style
var qtyStyle = {
    textAlign: "left",
    position: "relative",
    width: "10%",
    paddingRight: "10px"
}

var lineStyle = {
    overflow: "auto"
}

var modalImage = {
    minWidth: "90%",
    overflow: "auto",
    maxHeight: "100px"
}

//
// Tray Component
//
var Cart = React.createClass({
    getInitialState() {
        return { showModal: false, cartdetails: [] };
    },
    close() {
        this.setState({ showModal: false });
    },
    open() {
        this.setState({ showModal: true });
        var cart = this.props.cart;
        var url = this.props.source + "/" + cart.CartId;
        httpGet(url, function (data) {
            this.setState({ cartdetails: data });
        }.bind(this));
    },
    render: function () {
        var detailsForModal = this.state.cartdetails.map(details =>
            <ModalDetails details={details} key={details.CartId } />

);

return (
<div>
<ListGroupItem onClick={this.open}>
<span className="col-xs-3 text-left">{this.props.cart.CartId}</span>
<span className="col-xs-9 orderline">{
    formatDate(this.props.cart.DateCreated)}</span>
</ListGroupItem>
            <Modal show={this.state.showModal} onHide={this.close}>
<Modal.Header closeButton>
<Modal.Title>
<div>
<span className="col-xs-12">&nbsp;</span>
<span className="col-xs-3 text-center">Order#:{this.props.cart.CartId}</span>
<span className="col-xs-9 text-right xsmallFont">Date:{formatDate(this.props.cart.DateCreated)}</span>
</div>
<img src="../img/receipt.jpg" className="col-xs-12 col-sm-12" style={modalImage}/>
</Modal.Title>
</Modal.Header>
<Modal.Body>
<ListGroup>

<div className="col sm-4 col-xs-4 top10 bold titleCol">Product</div>
<div className="col-sm-2 col-xs-1 top10 bold titleCol">MSRP</div>
<div className="col-sm-1 col-xs-1 top10 bold titleCol">QtyS</div>
<div className="col-sm-1 col-xs-1 top10 bold titleCol">QtyO</div>
<div className="col-sm-1 col-xs-1 top10 bold titleCol">QtyB</div>
<div className="col-sm-3 col-xs-3 top10 bold titleCol">Extended</div>


{detailsForModal}
</ListGroup>
</Modal.Body>
<Modal.Footer>
<div className="text-right">
<span className="col-xs-10">Sub:</span>
<span className="col-xs-2">${this.props.cart.TotalMSRP.toFixed(2)}</span>
<span className="col-xs-10">Tax:</span>
<span className="col-xs-2">${this.props.cart.TotalTax.toFixed(2)}</span>
<span className="col-xs-10">Order Total:</span>
<span className="col-xs-2">${(+this.props.cart.TotalTax + +this.props.cart.TotalMSRP).toFixed(2)}</span>
</div>
</Modal.Footer>
            </Modal>
</div>
)
}
});
//
// TrayList Component
//
var Cartlist = React.createClass({
    getInitialState: function () {
        return ({ carts: [] });
    },
    componentDidMount: function () {
        httpGet(this.props.source, function (data) {
            this.setState({ carts: data });
        }.bind(this));
    },
    render: function () {
        var carts = this.state.carts.map(cart =>
            <Cart cart={cart} key={cart.CartId} source="/GetCartDetailsAsync" />
);
return (
<div className="top25">
<div className="col-sm-4 col-xs-1">&nbsp;</div>
<div className="col-sm-4 col-xs-12">
<div className="panel-title text-center">
<h3 className="title">Past Orders</h3>
<img className="smaller-img" src="/img/file.png" />
</div>
<div className="panel-body">
<div>
<div className="text-center navbar navbar-default" style={{
    top: "25px",
    position: "relative"
}
}>
<div className="col sm-4 col-xs-2" style={{
    top: "10px",
    position: "relative"
}
}>#</div>
<div className="col-sm-8 col-xs-10" style={{
    top: "10px",
    position: "relative"
}
}>Date</div>
</div>
<ListGroup>
{carts}
</ListGroup>
</div>
</div>
</div>
</div>
        )
}
});
// ModalDetails Component
//
var ModalDetails = React.createClass({
    render: function () {
        return (
        <ListGroupItem style= {lineStyle}>
    <span className="col sm-4 col-xs-4 bold">{this.props.details.ProductName}</span>
    <span className="col-sm-2 col-xs-1 bold">${this.props.details.MSRP.toFixed(2)}</span>
    <span className="col-sm-1 col-xs-1 bold">{this.props.details.QtyS}</span>
    <span className="col-sm-1 col-xs-1 bold">{this.props.details.QtyO}</span>
    <span className="col-sm-1 col-xs-1 bold">{this.props.details.QtyB}</span>
    <span className="col-sm-3 col-xs-3 bold">${this.props.details.Extended.toFixed(2)}</span>

    </ListGroupItem>
)
}
});

ReactDOM.render(
<Cartlist source="/GetCarts" />,
document.getElementById("case") // html tag
)