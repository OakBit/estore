﻿using Microsoft.AspNet.Http;
using Microsoft.AspNet.Http.Features;
using Microsoft.AspNet.Razor.TagHelpers;
using System;
using System.Text;
using ASPNetCase1.ViewModels;
namespace eStore.TagHelpers
{
    // You may need to install the Microsoft.AspNet.Razor.Runtime package into your project
    [HtmlTargetElement("catalogue", Attributes = GenreIdAttribute)]
    public class CatalogueHelper : TagHelper
    {
        private const string GenreIdAttribute = "genre";
        [HtmlAttributeName(GenreIdAttribute)]
        public string GenreId { get; set; }
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        public CatalogueHelper(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if (_session.GetObject<ProductViewModel[]>("catalogue") != null && Convert.ToInt32(GenreId) >= 0)
            {
                var innerHtml = new StringBuilder();
                ProductViewModel[] catalogue = _session.GetObject<ProductViewModel[]>("catalogue");
                innerHtml.Append("<div class=\"col-xs-12\" style=\"font-size:x-large;\"><span>Catalogue</span></div>");
                foreach (ProductViewModel item in catalogue)
                {
                    if (item.GenreId == Convert.ToInt32(GenreId) || Convert.ToInt32(GenreId) == 0)
                    {
                        innerHtml.Append("<div id=\"item\" class=\"col-sm-12 col-xs-12 text-center\" style=\"border:solid; padding-bottom:3px;\">");
                        innerHtml.Append("<div><p id=prodId" + item.ProductId + " data-description=\"" + item.Description + "\">");
                        innerHtml.Append("<span class=\"col-sm-12 col-xs-12\" style=\"font-size:large; padding:5px; background-color:#E1EAED;\">" + item.ProductName + "</span></p>");
                        innerHtml.Append("<span class=\"col-xs-4 col-sm-4\"><img src=\"/img/" + item.GraphicName + "\" /></span>");

                        innerHtml.Append("<span class=\"col-xs-3 col-sm-2 text-right\">Genre</span>");
                        innerHtml.Append("<span class=\"col-xs-5 col-sm-6 text-left\">" + item.GenreName + "</span>");

                        innerHtml.Append("<span class=\"col-xs-3 col-sm-2 text-right\">Author</span>");
                        innerHtml.Append("<span class=\"col-xs-5 col-sm-6 text-left\">" + item.AuthorName + "</span>");

                        innerHtml.Append("<span class=\"col-xs-3 col-sm-2 text-right\">Publisher</span>");
                        innerHtml.Append("<span class=\"col-xs-5 col-sm-6 text-left\">" + item.PublisherName + "</span>");

                        innerHtml.Append("<p class=\"text-left\" id=descr" + item.ProductId + ">" + item.Description + "</p></div>");
                        innerHtml.Append("<div class=\"col-xs-12 col-sm-12\" style=\"padding-bottom: 10px;\"> <br/><a href=\"#details_popup\" data-toggle=\"modal\" class=\"btn btn-default\"");
                        innerHtml.Append(" id=\"modalbtn\" data-id=\"" + item.ProductId + "\">Details</a>");
                        innerHtml.Append("<input type=\"hidden\" id=\"mproductName" + item.ProductId + "\" value=\"" + item.ProductName + "\"/>");
                        innerHtml.Append("<input type=\"hidden\" id=\"mgenre" + item.ProductId + "\" value=\"" + item.GenreName + "\"/>");
                        innerHtml.Append("<input type=\"hidden\" id=\"mauthor" + item.ProductId + "\" value=\"" + item.AuthorName + "\"/>");
                        innerHtml.Append("<input type=\"hidden\" id=\"mdescription" + item.ProductId + "\" value=\"" + item.Description + "\"/>");
                        innerHtml.Append("<input type=\"hidden\" id=\"mpublisher" + item.ProductId + "\" value=\"" + item.PublisherName + "\"/>");
                        innerHtml.Append("<input type=\"hidden\" id=\"mprice" + item.ProductId + "\" value=\"" + item.MSRP.ToString("N2") + "\"/>");
                        innerHtml.Append("<input type=\"hidden\" id=\"mgraphic" + item.ProductId + "\" value=\"" + item.GraphicName + "\"/>");
                        innerHtml.Append("<input type=\"hidden\" id=\"misbn10" + item.ProductId + "\" value=\"" + item.ISBN10 + "\"/>");
                        innerHtml.Append("<input type=\"hidden\" id=\"misbn13" + item.ProductId + "\" value=\"" + item.ISBN13 + "\"/></div></div>");

                    }
                }
                output.Content.SetHtmlContent(innerHtml.ToString());
            }
        }
    }
}