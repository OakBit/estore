﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using ASPNetCase1.Models;
using System.Net.Http;

namespace ASPNetCase1.Controllers
{
    public class DataController : Controller
    {
        AppDbContext _db;
        public DataController(AppDbContext context)
        {
            _db = context;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> Json()
        {
            ProductModel model = new ProductModel(_db);
            string rawJsonString = await getMenuItemJsonFromWeb();
            bool genresLoaded = model.loadGenres(rawJsonString);
            bool authorsLoaded = model.loadAuthors(rawJsonString);
            bool publishersLoaded = model.loadPublishers(rawJsonString);
            bool productsLoaded = model.loadProducts(rawJsonString);
            if (genresLoaded && productsLoaded && authorsLoaded && publishersLoaded)
                ViewBag.JsonLoadedMsg = "Json Loaded Successfully";
            else
                ViewBag.JsonLoadedMsg = "Json NOT Loaded";
            return View("Index");
        }
        private async Task<String> getMenuItemJsonFromWeb()
        {
            string url = "https://gist.githubusercontent.com/florboards/f1722070b72ce431c32d4ad316d9aba7/raw/ba8acfd8972179d5d3506efc97e68ff09d203dea/booksImport.json";
            var httpClient = new HttpClient();
            var response = await httpClient.GetAsync(url);
            var result = await response.Content.ReadAsStringAsync();
            return result;
        }

    }
}