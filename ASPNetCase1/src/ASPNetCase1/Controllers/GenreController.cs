﻿
using Microsoft.AspNet.Mvc;
using ASPNetCase1.Models;
using ASPNetCase1.ViewModels;
using Microsoft.AspNet.Http;
using System.Collections.Generic;
using System;
using ASPNetCase1.Utils;
namespace ASPNetCase1.Controllers
{
    public class GenreController : Controller
    {
        AppDbContext _db;
        public GenreController(AppDbContext context)
        {
            _db = context;
        }
        public IActionResult Index()
        {
            CatalogueViewModel vm = new CatalogueViewModel();
            // only build the catalogue once
            if (HttpContext.Session.GetObject<List<Genre>>(SessionVars.Genres) == null)
            {
                try
                {
                    GenreModel genModel = new GenreModel(_db);
                    // now load the categories
                    List<Genre> genres = genModel.GetAll();
                    HttpContext.Session.SetObject(SessionVars.Genres, genres);
                    vm.SetGenres(HttpContext.Session.GetObject<List<Genre>>(SessionVars.Genres));
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "Catalogue Problem - " + ex.Message;
                }
            }
            else
            {
                vm.SetGenres(HttpContext.Session.GetObject<List<Genre>>(SessionVars.Genres));
            }
            return View(vm);
        }
        public IActionResult SelectGenre(CatalogueViewModel vm)
        {
            GenreModel genModel = new GenreModel(_db);
            AuthorModel autModel = new AuthorModel(_db);
            PublisherModel pubModel = new PublisherModel(_db);
            ProductModel catalogueModel = new ProductModel(_db);
            List<Product> items = catalogueModel.GetAllByGenreId(vm.GenreId);
            List<ProductViewModel> vms = new List<ProductViewModel>();
            if (items.Count > 0)
            {
                foreach (Product item in items)
                {
                    ProductViewModel pvm = new ProductViewModel();
                    pvm.Qty = 0;
                    pvm.ProductName = item.ProductName;
                    pvm.GenreId = item.GenreId;
                    pvm.GenreName = genModel.GetGenreName(item.GenreId);
                    pvm.Description = item.Description;
                    pvm.ProductId = item.ProductId;
                    pvm.AuthorId = item.AuthorId; //Author and pub id may be unnecessary if name is retrieved here (unless sorting/searching through them becomes possible)
                    pvm.AuthorName = autModel.GetAuthorName(item.AuthorId);
                    pvm.PublisherId = item.PublisherId;
                    pvm.PublisherName = pubModel.GetPublisherName(item.PublisherId);
                    pvm.CostPrice = Convert.ToDecimal(item.CostPrice);
                    pvm.MSRP = Convert.ToDecimal(item.MSRP);
                    pvm.GraphicName = item.GraphicName;
                    pvm.QtyOnHand = item.QtyOnHand;
                    pvm.QtyOnBackOrder = item.QtyOnBackOrder;
                    pvm.ISBN10 = item.ISBN10;
                    pvm.ISBN13 = item.ISBN13;
                    vms.Add(pvm);
                }
                ProductViewModel[] myCatalogue = vms.ToArray();
                HttpContext.Session.SetObject(SessionVars.Catalogue, myCatalogue);
            }
            vm.SetGenres(HttpContext.Session.GetObject<List<Genre>>(SessionVars.Genres));
            return View("Index", vm);
        }
        [HttpPost]
        public ActionResult SelectItem(CatalogueViewModel vm)
        {
            Dictionary<string, object> cart;
            if (HttpContext.Session.GetObject<Dictionary<String, Object>>(SessionVars.Cart) == null)
            {
                cart = new Dictionary<string, object>();
            }
            else
            {
                cart = HttpContext.Session.GetObject<Dictionary<string, object>>(SessionVars.Cart);
            }
            ProductViewModel[] catalogue = HttpContext.Session.GetObject<ProductViewModel[]>(SessionVars.Catalogue);
            String retMsg = "";
            foreach (ProductViewModel item in catalogue)
            {
                if (item.ProductId == vm.ProductId)
                {
                    if (vm.Qty > 0) // update only selected item
                    {
                        item.Qty = vm.Qty;
                        retMsg = vm.Qty + " - item(s) Added!";
                        cart[item.ProductId] = item;
                    }
                    else
                    {
                        item.Qty = 0;
                        cart.Remove(item.ProductId);
                        retMsg = "item(s) Removed!";
                    }
                    vm.GenreId = item.GenreId;
                    break;
                }
            }
            ViewBag.AddMessage = retMsg;
            HttpContext.Session.SetObject(SessionVars.Cart, cart);
            vm.SetGenres(HttpContext.Session.GetObject<List<Genre>>(SessionVars.Genres));
            return View("Index", vm);
        }
    }
}