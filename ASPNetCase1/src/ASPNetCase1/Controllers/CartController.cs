﻿using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Http;
using ASPNetCase1.Utils;
using ASPNetCase1.Models;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;

namespace ASPNetCase1.Controllers
{
    public class CartController : Controller
    {

        AppDbContext _db;
        public CartController(AppDbContext context)
        {
            _db = context;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }
        public ActionResult ClearCart()
        {
            HttpContext.Session.Remove(SessionVars.Cart); // clear out current cart
            HttpContext.Session.SetString(SessionVars.Message, "Cart Emptied"); // clear out current cart once order has been placed
            return Redirect("/Home");
        }

        // Add the tray, pass the session variable info to the db
        public ActionResult AddOrder()
        {
            // they can't add a Tray if they're not logged on
            if (HttpContext.Session.GetString(SessionVars.User) == null)
            {
                return Redirect("/Login");
            }
            CartModel model = new CartModel(_db);
            int retVal = -1;
            string retMessage = "";
            try
            {
                Dictionary<string, object> cartItems = HttpContext.Session.GetObject<Dictionary<string, object>>(SessionVars.Cart);
                retVal = model.AddCart(cartItems, HttpContext.Session.GetString(SessionVars.User));
                if (retVal > 0) // Cart Added
                {
                    retMessage = "Order " + retVal + " Created!";
                }
                else // problem
                {
                    retMessage = "Order not added, try again later";
                }
            }
            catch (Exception ex) // big problem
            {
                retMessage = "Order was not created, try again later! - " + ex.Message;
            }
            HttpContext.Session.Remove(SessionVars.Cart); // clear out current tray once persisted
            HttpContext.Session.SetString(SessionVars.Message, retMessage);
            return Redirect("/Home");
        }

        public IActionResult List()
        {
            if (HttpContext.Session.GetString(SessionVars.User) == null)
            {
                return Redirect("/Login");
            }
            return View("List");
        }
        [Route("[action]")]
        public IActionResult GetCarts()
        {
            CartModel model = new CartModel(_db);
            return Ok(model.GetCarts(HttpContext.Session.GetString(SessionVars.User)));
        }

        [Route("[action]/{cid:int}")]
        public IActionResult GetCartDetails(int cid)
        {
            CartModel model = new CartModel(_db);
            return Ok(model.GetCartDetails(cid, HttpContext.Session.GetString(SessionVars.User)));
        }
        [Route("[action]/{cid:int}")]
        public async Task<IActionResult> GetCartDetailsAsync(int cid)
        {
            CartModel model = new CartModel(_db);
            List<ViewModels.CartViewModel> details = await model.GetCartDetailsAsync(cid, HttpContext.Session.GetString(SessionVars.User));
            return Ok(details);
        }
    }
}