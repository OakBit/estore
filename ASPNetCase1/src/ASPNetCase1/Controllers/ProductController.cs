﻿using Microsoft.AspNet.Mvc;
using ASPNetCase1.Models;
using ASPNetCase1.ViewModels;
namespace ASPNetCase1.Controllers
{
    public class ProductController : Controller
    {
        AppDbContext _db;
        public ProductController(AppDbContext context)
        {
            _db = context;
        }
        // GET: /<controller>/
        public IActionResult Index(GenreViewModel genre)
        {
            ProductModel model = new ProductModel(_db);
            ProductViewModel viewModel = new ProductViewModel();
            viewModel.GenreName = genre.GenreName;
            viewModel.Products = model.GetAllByGenreName(genre.GenreName);
            return View(viewModel);
        }
    }
}